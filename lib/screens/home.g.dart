// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductCategories _$ProductCategoriesFromJson(Map<String, dynamic> json) {
  return ProductCategories(
    Category.fromJson(json['category'] as Map<String, dynamic>),
    (json['products'] as List<dynamic>)
        .map((e) => Product.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ProductCategoriesToJson(ProductCategories instance) =>
    <String, dynamic>{
      'category': instance.category,
      'products': instance.products,
    };

Category _$CategoryFromJson(Map<String, dynamic> json) {
  return Category(
    json['categoryid'] as int,
    json['name'] as String,
  );
}

Map<String, dynamic> _$CategoryToJson(Category instance) => <String, dynamic>{
      'categoryid': instance.categoryid,
      'name': instance.name,
    };

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
    json['id'] as String,
    json['image'] as String,
    json['local_image'] as String,
    json['flag'] as int,
  );
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'id': instance.id,
      'image': instance.image,
      'local_image': instance.local_image,
      'flag': instance.flag,
    };
