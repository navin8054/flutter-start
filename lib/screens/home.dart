import 'dart:convert';
import 'dart:developer' as developer;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:json_annotation/json_annotation.dart';

part 'home.g.dart';

Future<List<ProductCategories>> fetchProduct() async {
  final response = await http
      .get(Uri.parse('https://fliegenglas.app/wp-json/sow/v6/getCategories'));

  List<ProductCategories> productList = [];
  if (response.statusCode == 200) {
    var jsonData = jsonDecode(response.body);
    // If the server did return a 200 OK response,
    // then parse the JSON.
    for (int i = 0; i < 20; i++) {
      productList.add(ProductCategories.fromJson(jsonData[i]));
    }
    return productList;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

@JsonSerializable()
class ProductCategories {
  Category category;

  List<Product> products;

  ProductCategories(this.category, this.products);

  factory ProductCategories.fromJson(Map<String, dynamic> json) =>
      _$ProductCategoriesFromJson(json);
}

@JsonSerializable()
class Category {
  int categoryid;
  String name;

  Category(this.categoryid, this.name);

  factory Category.fromJson(Map<String, dynamic> json) =>
      _$CategoryFromJson(json);
}

@JsonSerializable()
class Product {
  String id;
  String image;
  String local_image;
  int flag;

  Product(this.id, this.image, this.local_image, this.flag);

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  late Future<List<ProductCategories>> productCategories;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    productCategories = fetchProduct();
    developer.log('log me $productCategories');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Home"),
        ),
        body: Container(
            child: FutureBuilder<List<ProductCategories>>(
                future: fetchProduct(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    var data = snapshot.data;
                    return _categoryList(data);
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }
                  return CircularProgressIndicator();
                })));
  }

  ListView _categoryList(data) {
    return ListView.builder(
        itemCount: 4,
        itemBuilder: (context, index) {
          return Container(
            height: 260.0,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomRight,
                    colors: [Colors.black87, Colors.blueGrey.shade900])),
            child:
                Column(mainAxisAlignment: MainAxisAlignment.start, children: [
              Container(
                margin: EdgeInsets.only(right: 5, left: 3, top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                        child: Text(data[index].category.name,
                            style:
                                TextStyle(fontSize: 16, color: Colors.white))),
                    Text('Alle anzeigen',
                        style: TextStyle(fontSize: 16, color: Colors.white)),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 3, left: 3, top: 5),
                height: 200.0,
                child: _productList(data[index].products)
              ),
            ]),
          );
        });
  }

  ListView _productList(data) {
    return ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 5,
        itemBuilder: (context, index) {
          return Container(
            height: 150.0,
            margin: const EdgeInsets.only(right: 10),
            child: Image.network(data[index].image),
          );
        });
  }
}
