// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:flutter_app/screens/drawer.dart';
import 'package:flutter_app/screens/form.dart';
import 'package:flutter_app/screens/tabs.dart';

import './screens/home.dart';
import './screens/album.dart';
import './screens/example.dart';

void main() {
  runApp(MaterialApp(
    title: 'Navigation Basics',
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('App Root'),
      ),
      body: Column(
        children: [
          ElevatedButton(
            child: Text('Home'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Home()),
              );
            },
          ),
          ElevatedButton(
            child: Text('Example'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Example()),
              );
            },
          ),
          ElevatedButton(
            child: Text('Album'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MyAlbum()),
              );
            },
          ),
          ElevatedButton(
            child: Text('Form'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ExampleForm()),
              );
            },
          ),
          ElevatedButton(
            child: Text('Tabs'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => TabsExample()),
              );
            },
          ),
          ElevatedButton(
            child: Text('Drawer'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => DrawerExample()),
              );
            },
          ),
        ],
      ),
    );
  }
}
